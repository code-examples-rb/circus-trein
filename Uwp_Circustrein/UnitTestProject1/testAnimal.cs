﻿using System;
using System.Collections.Generic;
using Circustrein.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTestProject1
{
    [TestClass]
    public class testAnimal
    {
        private Animal animal;

        [TestInitialize]
        public void Setup()
        {
            animal = new Animal();
        }

        //Test if animal feels safe when he should feel safe
        [TestMethod]
        public void TestAnimalFeelsSafe()
        {
            animal.size = sizeAnimal.big;
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal() { size = sizeAnimal.small, type = typeAnimal.carnivore });
            animals.Add(new Animal() { size = sizeAnimal.big, type = typeAnimal.herbivore });

            var result = animal.feelsSafe(animals);

            Assert.IsTrue(result);
        }

        //Test if animal doesn't feel safe with a carnivore that has the same size as the animal
        [TestMethod]
        public void TestAnimalFeelsSafeSameSize()
        {
            animal.size = sizeAnimal.big;
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal() { size = sizeAnimal.small, type = typeAnimal.herbivore });
            animals.Add(new Animal() { size = sizeAnimal.big, type = typeAnimal.carnivore });

            var result = animal.feelsSafe(animals);

            Assert.IsFalse(result);
        }

        //Test if the animal doesn't feel safe with a carnivore that is bigger than the animal
        [TestMethod]
        public void TestAnimalFeelsSafeSmaller()
        {
            animal.size = sizeAnimal.small;
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal() { size = sizeAnimal.small, type = typeAnimal.herbivore });
            animals.Add(new Animal() { size = sizeAnimal.big, type = typeAnimal.carnivore });

            var result = animal.feelsSafe(animals);

            Assert.IsFalse(result);
        }

    }
}
