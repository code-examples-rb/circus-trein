﻿using System;
using System.Collections.Generic;
using Circustrein.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class testWagon
    {
        private Wagon wagon;

        [TestInitialize]
        public void Setup()
        {
            wagon = new Wagon();
        }

        //Test if you can add an animal when it's supposed to fit inside the wagon
        [TestMethod]
        public void TestCanAddAnimalTrue()
        {
            Animal animal = new Animal()
            {
                size = sizeAnimal.medium,
                type = typeAnimal.carnivore
            };

            var result = wagon.canAddAnimal(animal);

            Assert.IsTrue(result);
        }

        //Test if even if there is multiple animals currently in the wagon, the animal still fits in
        [TestMethod]
        public void TestCanAddAnimalWithMultiple()
        {
            Animal animal = new Animal()
            {
                size = sizeAnimal.medium,
                type = typeAnimal.carnivore
            };

            wagon.addAnimal(new Animal() { size = sizeAnimal.small, type = typeAnimal.herbivore });
            wagon.addAnimal(new Animal() { size = sizeAnimal.medium, type = typeAnimal.herbivore });
            wagon.addAnimal(new Animal() { size = sizeAnimal.medium, type = typeAnimal.herbivore });

            var result = wagon.canAddAnimal(animal);

            Assert.IsTrue(result);
        }

        //Test if you can't add an animal when it's too big but feels safe
        [TestMethod]
        public void TestCantAddAnimalTooBig()
        {
            Animal animal = new Animal()
            {
                size = sizeAnimal.medium,
                type = typeAnimal.carnivore
            };

            wagon.addAnimal(new Animal() { size = sizeAnimal.big, type = typeAnimal.herbivore });
            wagon.addAnimal(new Animal() { size = sizeAnimal.big, type = typeAnimal.herbivore });

            var result = wagon.canAddAnimal(animal);

            Assert.IsFalse(result);
        }

        //Test if you can't add an animal when it doesnt feel safe but does fit with size
        [TestMethod]
        public void TestCantAddAnimalNotSafe()
        {
            Animal animal = new Animal()
            {
                size = sizeAnimal.small,
                type = typeAnimal.carnivore
            };

            wagon.addAnimal(new Animal() { size = sizeAnimal.small, type = typeAnimal.herbivore });
            wagon.addAnimal(new Animal() { size = sizeAnimal.big, type = typeAnimal.carnivore });

            var result = wagon.canAddAnimal(animal);

            Assert.IsFalse(result);
        }
    }
}
