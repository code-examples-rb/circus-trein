﻿using System.Collections.Generic;

namespace Circustrein.Classes
{
    public class Wagon
    {
        private int max = 10;
        private int current = 0;
        public List<Animal> animals = new List<Animal>();

        //Add animal to the wagon
        public void addAnimal(Animal animal)
        {
            animals.Add(animal);
            animal.inWagon = true;
            updateCurrentWeight((int)animal.size);
        }

        private void updateCurrentWeight(int size)
        {
            current += size;
        }

        private bool enoughSpaceForAnimal(int size)
        {
            //Check the current + new size added is less than the max capacity.
            return (current + size <= max);
        }

        public bool canAddAnimal(Animal animal)
        {
            return (animal.feelsSafe(animals) && enoughSpaceForAnimal((int)animal.size));
        }

        //To show animals inside of the wagon listview
        public string wagonAnimals
        {
            get
            {
                string message = null;

                foreach (Animal animal in animals)
                {
                    message = message + animal.type + ":  " + animal.size + ", ";
                }

                return message;
            }
        }
    }
}
