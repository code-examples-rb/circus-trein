﻿using System.Collections.Generic;

namespace Circustrein.Classes
{
    public class Animal
    {
        public sizeAnimal size;
        public typeAnimal type;
        public bool inWagon = false;

        public bool feelsSafe(List<Animal> animalsInWagon)
        {
            bool isSafe = true;

            //Go through the list
            foreach (Animal animal in animalsInWagon)
            {
                //If there's a carnivore that's bigger or equal size, animal doesn't feel safe
                if (animal.type == typeAnimal.carnivore && animal.size >= size)
                {
                    isSafe = false;
                }
            }

            return isSafe;
        }

        //Get size for listview
        public string getSize
        {
            get
            {
                return size.ToString();
            }
        }

        //Get type for listview
        public string getType
        {
            get
            {
                return type.ToString();
            }
        }
    }
}
