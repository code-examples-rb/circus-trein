﻿#pragma checksum "D:\Study\S4\GIT\circustrein\Uwp_Circustrein\Circustrein\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F53DA8E11A52F6EC30393FEC724E1CA0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Circustrein
{
    partial class MainPage : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.17.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 2: // MainPage.xaml line 68
                {
                    this.lvWagons = (global::Windows.UI.Xaml.Controls.ListView)(target);
                }
                break;
            case 4: // MainPage.xaml line 39
                {
                    this.lvAnimals = (global::Windows.UI.Xaml.Controls.ListView)(target);
                }
                break;
            case 6: // MainPage.xaml line 33
                {
                    this.btnAddAnimal = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.btnAddAnimal).Tapped += this.BtnAddAnimal_Tapped;
                    ((global::Windows.UI.Xaml.Controls.Button)this.btnAddAnimal).Click += this.BtnAddAnimal_Click;
                }
                break;
            case 7: // MainPage.xaml line 34
                {
                    this.btnSort = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.btnSort).Tapped += this.BtnSort_Tapped;
                }
                break;
            case 8: // MainPage.xaml line 28
                {
                    this.cbSize = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            case 9: // MainPage.xaml line 31
                {
                    this.cbType = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        /// <summary>
        /// GetBindingConnector(int connectionId, object target)
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.17.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

