﻿using Circustrein.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Circustrein
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private List<Animal> animals    = new List<Animal>();
        private List<Wagon> wagons      = new List<Wagon>();
        public MainPage()
        {
            this.InitializeComponent();

            var enumSize = Enum.GetValues(typeof(sizeAnimal)).Cast<sizeAnimal>();
            var enumType = Enum.GetValues(typeof(typeAnimal)).Cast<typeAnimal>();

            cbSize.ItemsSource = enumSize.ToList();
            cbType.ItemsSource = enumType.ToList();
        }

        private void showData()
        {
            lvAnimals.ItemsSource   = null;
            lvAnimals.ItemsSource   = animals;


            lvWagons.ItemsSource    = null;
            lvWagons.ItemsSource    = wagons;
        }

        private void BtnAddAnimal_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Animal animal = new Animal() { type = (typeAnimal)cbType.SelectedItem, size = (sizeAnimal)cbSize.SelectedItem };
            animals.Add(animal);
            showData();
        }

        private void BtnSort_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Train train = new Train(animals);
            wagons = train.GetSortedAnimalsInWagons();

            showData();
        }

        private void BtnAddAnimal_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
